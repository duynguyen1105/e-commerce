import React, { useState } from 'react'
import { LoadingSpinner } from '../components';

const useLoadingSpinner = () => {
  const [loading, setLoading] = useState(false);
    return ([
        loading ? <LoadingSpinner/> : null,
        () => setLoading(true),
        () => setLoading(false)        
    ])
}

export default useLoadingSpinner
