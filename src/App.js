import React, { useEffect, useState } from 'react'
import { Products, Navbar, Cart, Checkout,  } from './components'
import { commerce } from './lib/commerce'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import useLoadingSpinner from './hooks/useLoadingSpinner'

export const App = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState({});
  const [loader, showLoader, hideLoader] = useLoadingSpinner();
  const [order, setOrder] = useState({});
  const [errorMessage, setErrorMessage] = useState('');
  
  const fetchProducts = async () => {
    // showLoader(); 
    const { data } = await commerce.products.list();
    // hideLoader();

    setProducts(data);
  };

  const fetchCart =  async () => {
    // showLoader();
    const cart = await commerce.cart.retrieve();
    // hideLoader();

    setCart(cart);
  }

  const handleAddToCart = async (productID, quanitity) => {
    showLoader();
    const {cart} = await commerce.cart.add(productID, quanitity);
    hideLoader();
    setCart(cart);
  }

  const handleUpdateCartQuantity = async (productID, quantity) =>{
    showLoader();
    const {cart} = await commerce.cart.update(productID, {quantity});
    hideLoader();
    setCart(cart);
  }

  const handleRemoveFromCart = async (productID) => {
    showLoader();
    const {cart} =await commerce.cart.remove(productID);
    hideLoader();

    setCart(cart);
  }

  const handleEmptyCart = async () => {
    showLoader();
    const {cart} = await commerce.cart.empty();
    // hideLoading();
    hideLoader();
    setCart(cart);
  }

  const refreshCart = async () => {
    const newCart = await commerce.cart.refresh();

    setCart(newCart);
  }

  const hadleCaptureCheckout = async (checkoutTokenId, newOrder) => {
    try {
      const incomingOrder = await commerce.checkout.capture(checkoutTokenId, newOrder);

      setOrder(incomingOrder);
      refreshCart(); 

    } catch (error) {
      setErrorMessage(error.data.error.message);
    }
  }


  useEffect(() => {
    fetchProducts();
    fetchCart();
  },[]);


  return (
    <Router>
      <div>
        <Navbar totalItems={cart.total_items}/>
        <Switch>
          <Route exact path="/">
            <Products products={products} onAddToCart={handleAddToCart}/>
          </Route>
          <Route exact path="/cart">
            <Cart 
              cart={cart}
              handleUpdateCartQuantity={handleUpdateCartQuantity}
              handleRemoveFromCart={handleRemoveFromCart}
              handleEmptyCart={handleEmptyCart}
            /> 
          </Route>
          <Route exact path="/checkout">
            <Checkout 
              cart={cart}
              order={order}
              onCaptureCheckout={hadleCaptureCheckout}  
              error={errorMessage}
            />
          </Route>
        </Switch>
      </div>
      {loader}
    </Router>

  )
}


export default App;


