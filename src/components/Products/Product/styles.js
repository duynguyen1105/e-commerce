import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 'auto',
    paddingTop: '56.25%',
    backgroundSize: 'contain',
  },
  title: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  cardActions: {
    display: 'flex',
  },
  price: {
    flexGrow: '1',
  },
}));