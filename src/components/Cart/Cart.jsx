import useStyles from './styles'
import { Button, Container, Grid, Typography } from '@material-ui/core'
import React from 'react'
import CartItem from './CartItem/CartItem';
import { Link } from 'react-router-dom';

function Cart({cart, handleUpdateCartQuantity, handleRemoveFromCart, handleEmptyCart}) {
    const classes = useStyles();

    const EmptyCart = () => (
        <Typography variant="subtitle1" gutterBottom>You have no item in your shopping cart 
        <br/>
            <Link to='/' className={classes.link}>
                <Button size="large" type="button" variant="contained" color="primary">GO SHOPPING !!!</Button>
            </Link>
        </Typography>

    );

    if (!cart.line_items) return 'Loading';

    const FilledCart = () => (
        <>
            <Grid container spacing={3}>
                {cart.line_items.map((item)=>(
                    <Grid item xs={12} sm={4} key={item.id}>
                        <CartItem item={item} handleUpdateCartQuantity={handleUpdateCartQuantity} handleRemoveFromCart={handleRemoveFromCart}  />
                    </Grid>
                ))}
            </Grid>
            <div className={classes.cardDetails}>
                    <Typography variant="h4">
                        Subtotal: {cart.subtotal.formatted_with_symbol}
                    </Typography>
                    <div>
                        <Button className={classes.emptyButton} onClick={handleEmptyCart} size="large" type="button" variant="contained" color="secondary" >
                            Empty Cart
                        </Button>
                        <Button component={Link} to='/checkout' className={classes.checkoutButton} size="large" type="button" variant="contained" color="primary">
                            Checkout
                        </Button>
                    </div>
            </div>
        </>
    )
    

    return (
        <Container>
            <div className={classes.toolbar}/>
            <Typography className={classes.title} variant="h3" gutterBottom>Your Shopping Cart</Typography>
            {!cart.line_items.length ? <EmptyCart/> : <FilledCart/>}
        </Container>
    )
}

export default Cart



