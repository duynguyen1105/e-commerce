import { CircularProgress } from '@material-ui/core'
import React from 'react'
import useStyles from './styles'


const LoadingSpinner = () => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <CircularProgress className={classes.loading} size={100}/>
        </div>
    )
}

export default LoadingSpinner
