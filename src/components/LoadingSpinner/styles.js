import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    container: {
        // margin: '0 auto',
        position:'fixed',
        width: '100%',
        height: '100%',
        left: '0px',
        top: '0px',
        background: 'gray',
        opacity:'0.5'
    },
    loading: {
        position: 'absolute',
        zIndex: '1000',
        left: '47%',
        top: '40%',
    }
}));